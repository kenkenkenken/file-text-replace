import groovy.io.FileType
import groovy.time.TimeCategory

static void main(String[] args) {
  def timeStart = new Date()
  // TODO: def backupPath = "backup/"
  if (args.length >= 3) {
    def path = args[0]
    def searchString = args[1]
    def replaceString = args[2]
    println( "Starting replacing '" + searchString + "' with '" + replaceString + "' in directory " + path)
    def outputPath = null
    def changedFiles = null
    if (args.length == 4) {
      outputPath = args[3]
      changedFiles = []
      println("Outputting changed files to " + outputPath)
    }
    def list = []
    def dir = new File(path)
    //list all files
    dir.eachFileRecurse (FileType.FILES) { file ->
      list << file
    }
    //iterate lines of each files and replace texts
    list.each {
      def strings = new File(it.path)
      def lines = strings.readLines()
      def replacedLines = []
      lines.forEach {
        replacedLines.add(it.replace(searchString, replaceString))
      }
      new File(it.path).withWriter { w ->
        w << replacedLines.join()
      }
      // list all changed files to be outputted on the provided file path
      if (lines != replacedLines && outputPath != null) {
        changedFiles.add(it.path)
      }
    }
    //write changed files to provided output file
    if (outputPath != null && changedFiles.size() > 0) {
      new File(outputPath).withWriter { w ->
        w << changedFiles.join("\n")
      }
    }
  } else {
    println("Insufficient params")
  }
  def timeStop = new Date()
  println("Process took " + TimeCategory.minus(timeStop, timeStart) + " to complete")

}



