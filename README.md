# file-text-replace
Simple groovy app that parses directories that contains with text files only


## How to run the app?

To run the application (in root path):
- Accepts string params in order:
  1. test = path of the folder to traverse in relation to root path
  2. Hi = string to search for and replace
  3. Hello = string to replace all search string in param 2
  4. (optional) output = if you want to list all changed files

```
groovy src/file-text-replace.groovy "test" "Hi" "Hello" "output"
```

## To do

- [ ] Performance Improvements
- [ ] Error handling
  - [ ] File type checking
  - [ ] handle same filenames when doing backup
  - [ ] rollback of list when error occurred while processing 